<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Helper\ProgressBar;
use AppBundle\Service\FileReader\CsvFileReader;
use AppBundle\Entity\PostalCodeFioul;

class ImportPostalCodeFioulAmountCommand extends ContainerAwareCommand
{
    protected $counterRow = 0;

    protected function configure()
    {
         $this->setName('app:import-postal-code-fioul-amount')
            ->setDescription('Import postal code fioul amount')
            ->setHelp('This command import postal code fioul amount from csv file');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln([
            'Start import',
            '============',
            '',
        ]);

        // Progress bar
        $progress = new ProgressBar($output, 1999999);
        $progress->start();

        $csvFileReader = $this->getContainer()->get('AppBundle\Service\FileReader\CsvFileReader');
        $csvFileReader->open(realpath($this->getContainer()->get('kernel')->getRootDir().'/../var/files/prices.csv'));
        $entityManager = $this->getContainer()->get('doctrine.orm.entity_manager');
        $entityManager->getConnection()->getConfiguration()->setSQLLogger(null);

        foreach($csvFileReader->read() as $row) {
            $this->addRow($entityManager, $row);
            // advance the progress bar 1 unit
            $progress->advance();
            $this->counterRow ++;
        }

        $entityManager->flush();
        $entityManager->clear();
        $csvFileReader->close();

        $progress->finish();

        $output->writeln([
            'End import',
            '============',
            '',
        ]);
    }

    private function addRow($entityManager, $row)
    {
        $postalCodeFioul = new PostalCodeFioul();
        $postalCodeFioul->setPostalCodeId($row[0]);
        $postalCodeFioul->setAmount($row[1]);
        $postalCodeFioul->setCreatedAt($row[2]);

        $entityManager->persist($postalCodeFioul);

        if($this->counterRow % 500 === 0) {
            $entityManager->flush();
            $entityManager->clear();
        }
    }
}