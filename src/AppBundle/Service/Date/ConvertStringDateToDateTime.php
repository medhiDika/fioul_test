<?php

namespace AppBundle\Service\Date;


class ConvertStringDateToDateTime
{
    /**
     * @var DateTime
     *
     */
    protected $date;

    /**
     *
     * Get date
     *
     * @return DateTime
     *
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     *
     * Convert string date to datetime
     *
     * @param string $date
     * @return boolean
     */
    public function convert(string $date)
    {
        try {
            $this->date = new \DateTime($date);
        } catch (\Exception $e) {
            return false;
        }

        return true;
    }
}