<?php

namespace AppBundle\Service\FileReader;

interface FileReaderInterface
{
    /**
     *
     * Check file exist
     *
     * @param string $fileName
     */
    public function checkFileExist($fileName);

    /**
     *
     * Open file
     *
     * @param string $fileName
     */
    public function open($fileName);

    /**
     *
     * Read file data
     *
     */
    public function read();

    /**
     *
     * Close file data
     *
     */
    public function close();
}