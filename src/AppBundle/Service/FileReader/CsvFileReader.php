<?php

namespace AppBundle\Service\FileReader;

use AppBundle\Exception\CsvFileReaderException;

class CsvFileReader implements FileReaderInterface
{

    protected $delimiter = ',';
    protected $handle;
    protected $removeHeader = true;

    /**
     *
     * Check file exist
     *
     * @param string $fileName
     * @return boolean
     */
    public function checkFileExist($fileName)
    {
        return (file_exists($fileName) === true and is_readable($fileName) === true);
    }

    /**
     *
     * Open file
     *
     * @param string $fileName
     */
    public function open($fileName)
    {
        if($this->checkFileExist($fileName) === false) {
            throw new CsvFileReaderException("File doesn't exist or is not readable", 1);
        }

        if (($this->handle = fopen($fileName, "r")) === FALSE) {
            throw new CsvFileReaderException("File open error", 1);
        }
    }

    /**
     *
     * Read file data
     *
     */
    public function read()
    {
        $counterRow = 0;
        
        while (($data = fgetcsv($this->handle, 1000, $this->delimiter)) !== FALSE) {
            $counterRow ++;
            if($counterRow === 1 and $this->removeHeader === true) {
                continue;
            }
            yield $data;
        }
    }

    /**
     *
     * Close file data
     *
     */
    public function close()
    {
        fclose($this->handle);
    }
}