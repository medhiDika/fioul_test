<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PostalCodeFioul
 *
 * @ORM\Table(name="postal_code_fioul", indexes={@ORM\Index(name="postal_code_id_created_at_index", columns={"postal_code_id", "created_at"})})
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PostalCodeFioulRepository")
 */
class PostalCodeFioul
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="postal_code_id", type="bigint")
     */
    private $postalCodeId;

    /**
     * @var float
     *
     * @ORM\Column(name="amount", type="float")
     */
    private $amount;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="date")
     */
    private $createdAt;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set postalCodeId
     *
     * @param integer $postalCodeId
     *
     * @return PostalCodeFioul
     */
    public function setPostalCodeId($postalCodeId)
    {
        $this->postalCodeId = $postalCodeId;

        return $this;
    }

    /**
     * Get postalCodeId
     *
     * @return int
     */
    public function getPostalCodeId()
    {
        return $this->postalCodeId;
    }

    /**
     * Set amount
     *
     * @param float $amount
     *
     * @return PostalCodeFioul
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set createdAt
     *
     * @param \Date $createdAt
     *
     * @return PostalCodeFioul
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = new \DateTime($createdAt);

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \Date
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }
}

