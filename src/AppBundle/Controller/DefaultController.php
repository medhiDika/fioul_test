<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\HttpFoundation\JsonResponse;
use AppBundle\Entity\PostalCodeFioul;
use AppBundle\Service\Date\ConvertStringDateToDateTime;

class DefaultController extends BaseController
{
    /**
     * @Route("/api/fioul/postalCodeId/{postalCodeId}/date1/{date1}/date2/{date2}", methods={"GET","HEAD"}, requirements={"postalCodeId"="\d+","date1"="[0-9]{4}\-[0-9]{2}\-[0-9]{2}"})
     */
    public function getFioulByPostalCodeIdAndDatesAction(ConvertStringDateToDateTime $convertStringDateToDateTime, $postalCodeId, $date1, $date2)
    {
        if($convertStringDateToDateTime->convert($date1) === false) {
            return $this->respondJsonUnauthorized(['invalid_date_1']);
        }
        $date1 = $convertStringDateToDateTime->getDate();

        if($convertStringDateToDateTime->convert($date2) === false) {
            return $this->respondJsonUnauthorized(['invalid_date_2']);
        }
        $date2 = $convertStringDateToDateTime->getDate();

        $postalCodeFioul = $this->getDoctrine()
            ->getRepository(PostalCodeFioul::class)
            ->findAllByPostalCodeIdBetweenDates($postalCodeId, $date1, $date2);

        return $this->respondJsonOk([$postalCodeFioul]);
    }
}
