<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class BaseController extends Controller
{
    /**
     *
     * Respond json data
     *
     * @param array $data
     * @param int $statusCode
     *
     * @return Response
     */
    private function respondJson(array $data, int $statusCode)
    {
        $response = new Response();
        $response->setContent(json_encode($data));
        $response->headers->set('Content-Type', 'application/json');
        $response->headers->set('status', $statusCode);

        return $response;
    }

    /**
     *
     * Respond json data for ok
     *
     * @param array $data
     *
     * @return Response
     */
    protected function respondJsonOk($data)
    {
        return $this->respondJson($data, Response::HTTP_OK);
    }

    /**
     *
     * Respond json data for unauthorized 
     *
     * @param array $data
     *
     * @return Response
     */
    protected function respondJsonUnauthorized($data)
    {
        return $this->respondJson(['error' => $data], Response::HTTP_UNAUTHORIZED);
    }
}
