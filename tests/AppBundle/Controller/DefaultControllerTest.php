<?php

namespace Tests\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class DefaultControllerTest extends WebTestCase
{
    public function testGetFioulAmountByDates()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/api/fioul/postalCodeId/1/date1/2015-08-01/date2/2015-08-06');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }
}
